const gameContainer = document.getElementById("game");
const load = document.addEventListener("DOMContentLoaded", () => {
    const lastScore = JSON.parse(localStorage.getItem("score"))
    let highTag = document.getElementById("high")
    highTag.innerHTML = lastScore
    document.getElementById("dis").style.display = "none"
})

const GIFS1 = [
    'url(./gifs/1.gif)',
    'url(./gifs/2.gif)',
    'url(./gifs/3.gif)',
    'url(./gifs/4.gif)',
    'url(./gifs/5.gif)',
    'url(./gifs/6.gif)',
    'url(./gifs/7.gif)'
]

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

// let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(gifs) {
    for (let gif of gifs) {
        // create a new div
        const newDiv = document.createElement("div");

        // give it a class attribute for the value we are looping over
        newDiv.classList.add(gif);

        // call a function handleCardClick when a div is clicked on
        newDiv.addEventListener("click", handleCardClick);

        // append the div to the element with an id of game
        gameContainer.append(newDiv);

    }
}
let result = 0;
let clicks = 0;
let cards = []
// let easy1 = Math.floor(GIFS1.length / 2);
// let medium1 = (GIFS1.length) - 2;
// let hard1 = (GIFS1.length);
// TODO: Implement this function!

function handleCardClick(event) {
    // you can use event.target to see which element was clicked
    console.log(event, "ok")
    cards.push(event);
    console.log(event.target, "event")
    console.log(level, "levels")
    // helps to click the card only one time


    if (cards.length <= 2) {
        // event.target.style.backgroundColor = event.target.className;
        event.target.style.backgroundImage = event.target.className;
        event.target.style.backgroundSize = "100% 100%";
        cards[0].target.removeEventListener("click", handleCardClick);
        clicks++;
        const scores = document.getElementById("score")
        scores.innerHTML = clicks;
        // ck.push(clicks)
        console.log(clicks)
    }
    // Compares the two cards colors/event
    if (cards.length == 2) {
        // console.log(event.target.className, "entered")
        if (cards[0].target.className == cards[1].target.className) {
            // console.log("Same cards");
            cards[0].target.removeEventListener("click", handleCardClick);
            cards[1].target.removeEventListener("click", handleCardClick);
            cards.length = 0; //used to empty the cards array.
            // console.log(cards);
            result++;
            //conditon to enter into the gameover function
            if (result == 3 || result == 5 || result == 7) {
                gameOver(level[0])
            }


        } else {
            // sets the delay for cards to click
            setTimeout(() => {
                console.log("entered into else")
                console.log(event.target.style.backgroundImage)
                cards[0].target.addEventListener("click", handleCardClick);
                cards[0].target.style.backgroundImage = "url('mem.jpeg')";
                cards[1].target.style.backgroundImage = "url('mem.jpeg')";
                cards.length = 0;
            }, 500)
        }
        localStorage.setItem("score", clicks)
    }
    console.log("you clicked", event.target);
}

// when the DOM loads

//start game fn
function startGame() {
    // let shuffledGifs = shuffle(GIFS);
    // createDivsForColors(shuffledGifs);
    document.getElementById("start").onclick = "null";
    document.getElementById("dis").style.display = "inline-block"
}

//global level. used in the handleCardsClick fn
const level = [];
//level selection 
function levels(event) {
    const levels = event.target.innerText;
    level.push(levels)
    if (level == "Easy") {
        noClick()
        console.log(level)
        console.log("level-easy")
        let easy = Math.floor(GIFS1.length / 2);
        console.log(easy)
        console.log(GIFS1)
        let newgif = []
        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < easy; j++) {
                newgif.push(GIFS1[j])
            }
        }
        console.log("new data set", newgif)
        let shuffledGifs = shuffle(newgif);
        createDivsForColors(shuffledGifs);
        newgif.length = 0;
    }
    if (level == "Medium") {
        noClick();
        console.log("level-medium")
        let medium = (GIFS1.length) - 2;
        // console.log(easy)
        console.log(GIFS1)
        let newgif = []
        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < medium; j++) {
                newgif.push(GIFS1[j])
            }
        }
        console.log("new data set", newgif)
        let shuffledGifs = shuffle(newgif);
        createDivsForColors(shuffledGifs);
        newgif.length = 0;
    }
    if (level == "Hard") {
        noClick();
        console.log("level-hard")
        let hard = (GIFS1.length);
        // console.log(easy)
        console.log(GIFS1)
        let newgif = []
        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < hard; j++) {
                newgif.push(GIFS1[j])
            }
        }
        console.log("new data set", newgif)
        let shuffledGifs = shuffle(newgif);
        createDivsForColors(shuffledGifs);
        newgif.length = 0;
    }

}

// displays the gameover message.
function gameOver(level) {
    switch (level) {
        case "Easy":
            if (result == 3) {
                alert("Game Over" + clicks)
                location.reload();
            }
        case "Medium":
            if (result == 5) {
                alert("Game Over" + clicks)
                location.reload();
            }
        case "Hard":
            if (result == 7) {
                alert("Game Over" + clicks)
                location.reload();
            }
    }

}

//disable the buttons when levels are selected 
function noClick() {
    document.getElementById("easy-button").onclick = "null"
    document.getElementById("medium-button").onclick = "null"
    document.getElementById("hard-button").onclick = "null"
    document.getElementById("h5").style.display = "none"
    document.getElementById("easy-button").style.display = "none"
    document.getElementById("medium-button").style.display = "none"
    document.getElementById("hard-button").style.display = "none"
}
//reset
function resetGame() {
    location.reload();
}



